from pyfme.aircrafts import Cessna172

''' Create Aircraft '''
aircraft = Cessna172()
print(f"Aircraft mass: {aircraft.mass} kg")
print(f"Aircraft inertia tensor: \n {aircraft.inertia} kg/m²")

print(f"forces: {aircraft.total_forces} N")
print(f"moments: {aircraft.total_moments} N·m")

print(aircraft.controls)
print(aircraft.control_limits)

''' Create Environment '''
from pyfme.environment.atmosphere import ISA1976
from pyfme.environment.wind import NoWind
from pyfme.environment.gravity import VerticalConstant

atmosphere = ISA1976()
gravity = VerticalConstant()
wind = NoWind()

from pyfme.environment import Environment
environment = Environment(atmosphere, gravity, wind)

''' Set Start State '''
from pyfme.models.state.position import EarthPosition
from pyfme.utils.trimmer import steady_state_trim

pos = EarthPosition(x=0, y=0, height=1000)
psi = 0.5  # rad
TAS = 45  # m/s
controls0 = {'delta_elevator': 0, 'delta_aileron': 0, 'delta_rudder': 0, 'delta_t': 0.5}

trimmed_state, trimmed_controls = steady_state_trim(
    aircraft,
    environment,
    pos,
    psi,
    TAS,
    controls0
)

# Environment conditions for the current state:
environment.update(trimmed_state)

# Forces and moments calculation:
forces, moments = aircraft.calculate_forces_and_moments(trimmed_state, environment, controls0)

''' Simulate '''
from pyfme.models import EulerFlatEarth

system = EulerFlatEarth(t0=0, full_state=trimmed_state)

''' Keep copntrols constant '''
from pyfme.utils.input_generator import Constant

controls = controls = {
    'delta_elevator': Constant(trimmed_controls['delta_elevator']),
    'delta_aileron': Constant(trimmed_controls['delta_aileron']),
    'delta_rudder': Constant(trimmed_controls['delta_rudder']),
    'delta_t': Constant(trimmed_controls['delta_t'])
}

from pyfme.simulator import Simulation

sim = Simulation(aircraft, system, environment, controls)

results = sim.propagate(10)

''' Render '''
import renderer
renderer = renderer.Renderer(results)
renderer.play()