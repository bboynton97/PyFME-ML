import time
import numpy as np
from sympy import sqrt, lambdify, symbols, solve, sin, cos

timesteps = 1000

x, y, z, t = symbols('x y z t')

xt = t**2
yt = t
zt = 1.1*t

print("Starting first test")
first_start_time = time.time()
for i in range(0, timesteps):
    distance = sqrt((xt - 1)**2 + (yt - 1)**2 + (zt - 13)**2) # TODO: rand values
    distance_prime = distance.diff(t)
    distance_prime_prime = distance_prime.diff(t)
    d = lambdify(t, distance)
    dp = lambdify(t, distance_prime)
    dpp = lambdify(t, distance_prime_prime)
    extreme_values = solve(distance_prime, t)
first_end_time = time.time()
print("First test completed")

distance = sqrt((xt - x)**2 + (yt - y)**2 + (zt - z)**2)
distance_prime = distance.diff(t)
distance_prime_prime = distance_prime.diff(t)
extreme_values = solve(distance_prime, t)
extreme_value_equation = extreme_values[0]
ev = lambdify((x, y, z), extreme_value_equation)

print("Starting second test")
second_start_time = time.time()
for i in range(0, timesteps):
    ev(1, 1, 13)
second_end_time = time.time()
print("Second test completed\n")

print("First: ", first_end_time - first_start_time)
print("Second: ", second_end_time - second_start_time)
