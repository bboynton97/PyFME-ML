"""
Straight and level flight training
"""

import gym
import gym_pyfme
import os

import time
import matplotlib.pyplot as plt
import math
import numpy as np

import models
import paths

from sympy import symbols


disp = []
env = gym.make('PyFME-v0')
model = models.PolicyGradientContinuous()  # or: DQN(), PolicyGradient(), or Custom()
path = paths.LevelClimb()

print("Training {} on {}".format(model.name, path.name))

# Set starting controls
action = [0, 0, 0, 0]

# Wipe previous render folder
os.system('rm -rf renders/*')

# Create path
env.create_path(*path.path)  # Choose path
gamma = 0.9
epsilon = .95

trials = 1000
trial_len = 500

render_every = 20

trial_rewards = []
for trial in range(trials):
    print("Trial:", trial)
    cur_state = env.reset()
    reward_history = []
    for step in range(trial_len):
        print("Step:", step)

        action = model.act(cur_state)  # Predict the next best action
        observation, reward, done, info = env.step(action)  # See what happens with that action

        reward_history.append(reward)
        print("Action:", action)

        # reward = reward if not done else -20
        new_state = observation

        model.remember(cur_state, action, reward)  # Remember what happened this iteration

        # if step % 5 == 0 and step != 0:
        #     model.fit()  # Learn from what happened

        cur_state = new_state

        # env.print_state()
        if trial % render_every == 0:
            env.render()
        if done:
            print('*************\n------ Done was triggered ------\n*************')
            print('Trial', trial, "got average reward", np.mean(reward_history[-100:]))
            trial_rewards.append(np.mean(reward_history[-100:]))
            break

    if trial % render_every == 0:
        env.export_render()
        os.system('mv render.gif renders/{}.gif'.format(trial))
        env.reset_render()

    model.fit()  # Fit at the end of the episode

print("Trial rewards:", trial_rewards)
results = env.get_results()

print("Closing env")
env.close()

