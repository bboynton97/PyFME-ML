from keras.layers import Dense, Activation, Input
from keras.models import Model, load_model
from keras.optimizers import Adam
import keras.backend as K
import numpy as np

class PolicyGradientContinuous(object):
    name = None

    def __init__(self, ALPHA=0.01, GAMMA=0.98, n_actions=4, layer1_size=64, layer2_size=128, input_dims=35, fname='pg.h5'):
        self.lr = ALPHA
        self.gamma = GAMMA
        self.n_actions = n_actions
        self.fc1_dims = layer1_size
        self.fc2_dims = layer2_size
        self.input_dims = input_dims
        self.G = 0

        self.name = self.name()

        # cleared out each episode
        self.state_memory = []
        self.action_memory = []
        self.reward_memory = []

        self.policy, self.predict = self.__build_policy_network()
        self.action_space = [i for i in range(n_actions)]
        self.model_file = fname

    def __build_policy_network(self):
        input = Input(shape=(self.input_dims,))
        advantages = Input(shape=[1])
        dense1 = Dense(self.fc1_dims, activation='relu')(input)
        dense2 = Dense(self.fc2_dims, activation='relu')(dense1)
        dense3 = Dense(64, activation='relu')(dense2)
        dense4 = Dense(32, activation='relu')(dense3)
        probs = Dense(self.n_actions, activation='softmax')(dense4)

        def custom_loss(y_true, y_pred):
            out = K.clip(y_pred, 1e-8, 1-1e-8)
            log_lik = y_true*K.log(out)
            return K.sum(-log_lik*advantages)

        policy = Model(input=[input, advantages], output=[probs])
        policy.compile(optimizer=Adam(lr=self.lr), loss=custom_loss)

        predict = Model(input=[input], output=[probs])

        return policy, predict

    def act(self, observation):
        state = observation[np.newaxis, :]
        probabilities = self.predict.predict(state)[0]
        action_change = np.random.choice(self.action_space, p=probabilities)

        # Convert 8-length single action into 4-length action array
        new_action = [0, 0, 0, 0]
        if action_change == 0:
            new_action[0] = 1
        elif action_change == 1:
            new_action[1] = 1
        elif action_change == 2:
            new_action[2] = 1
        elif action_change == 3:
            new_action[3] = 10
        elif action_change == 4:
            new_action[0] = -1
        elif action_change == 5:
            new_action[1] = -1
        elif action_change == 6:
            new_action[2] = -1
        elif action_change == 7:
            new_action[3] = -10

        return new_action

    def remember(self, observation, action, reward):
        self.state_memory.append(observation)
        self.action_memory.append(action)
        self.reward_memory.append(reward)

    def fit(self):
        state_memory = np.array(self.state_memory)
        action_memory = np.array(self.action_memory)
        reward_memory = np.array(self.reward_memory)

        # print("action memory", action_memory)
        # actions = np.zeros([len(action_memory), self.n_actions])
        # print("actions", actions)
        # actions[np.arange(len(action_memory)), action_memory] = 1

        actions = action_memory

        # print("Reward memory:", reward_memory)

        G = np.zeros_like(reward_memory)
        for t in range(len(reward_memory)):
            G_sum = 0
            discount = 1
            for k in range(t, len(reward_memory)):
                G_sum += reward_memory[k]*discount
                discount *= self.gamma
            G[t] = G_sum
        mean = np.mean(G)
        G = np.array(G, dtype=np.float64)  # Convert to np float instead of sympy float
        std = 1
        if np.std(G) > 0:
            std = np.std(G)
        # std = np.std(G) if np.std(G) > 0 else 1
        self.G = (G-mean)/std

        cost = self.policy.train_on_batch([state_memory, self.G], actions)

        self.state_memory = []
        self.action_memory = []
        self.reward_memory = []

    def save_model(self):
        self.policy.save(self.model_file)

    def load_model(self):
        self.policy.load_model(self.model_file)

    @staticmethod
    def name():
        return 'Policy Gradient Continuous'
