import numpy as np
import random
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam

from collections import deque


class DQN:
    name = None

    def __init__(self):
        self.memory = deque(maxlen=2000)

        self.gamma = 0.85
        self.epsilon = 1.0
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.005
        self.tau = .125
        self.name = self.name()

        self.model = self.create_model()
        self.target_model = self.create_model()

    def create_model(self):
        model = Sequential()
        model.add(Dense(24, input_dim=34, activation="relu"))  # 34 state inputs
        model.add(Dense(48, activation="relu"))
        model.add(Dense(24, activation="relu"))
        model.add(Dense(8))  # 4 control settings
        model.compile(loss="mean_squared_error",
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def act(self, state):
        self.epsilon *= self.epsilon_decay
        self.epsilon = max(self.epsilon_min, self.epsilon)
        action_change = 0
        if np.random.random() < self.epsilon:
            action_change = np.random.randint(7)
        print("inputting state:", state, "with shape", state.shape)
        action_change = np.argmax(self.model.predict(state))

        new_action = [0, 0, 0, 0]
        if action_change == 0:
            new_action[0] = 1
        elif action_change == 1:
            new_action[1] = 1
        elif action_change == 2:
            new_action[2] = 1
        elif action_change == 3:
            new_action[3] = 10
        elif action_change == 4:
            new_action[0] = -1
        elif action_change == 5:
            new_action[1] = -1
        elif action_change == 6:
            new_action[2] = -1
        elif action_change == 7:
            new_action[3] = -10

        return new_action

    def fit(self, state, action, reward, new_state, done):
        self.memory.append([state, action, reward, new_state, done])
        self.replay()
        self.target_train()

    def replay(self):
        batch_size = 32
        if len(self.memory) < batch_size:
            return

        samples = random.sample(self.memory, batch_size)
        for sample in samples:
            state, action, reward, new_state, done = sample
            target = self.target_model.predict(state)
            if done:
                target[0][action] = reward
            else:
                Q_future = max(self.target_model.predict(new_state)[0])
                target[0][action] = reward + Q_future * self.gamma
            self.model.fit(state, target, epochs=1, verbose=0)

    def target_train(self):
        weights = self.model.get_weights()
        target_weights = self.target_model.get_weights()
        for i in range(len(target_weights)):
            target_weights[i] = weights[i] * self.tau + target_weights[i] * (1 - self.tau)
        self.target_model.set_weights(target_weights)

    def save_model(self, fn):
        self.model.save(fn)

    @staticmethod
    def name():
        return 'DQN'
