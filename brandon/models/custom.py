import numpy as np
import random
from keras import layers
from keras.models import Model, Sequential
from keras.optimizers import SGD, Adam
from keras import backend as K
from keras import utils as np_utils
from keras import optimizers

from collections import deque


class Custom:
    name = None

    def __init__(self):
        self.input_dim = 35
        self.output_dim = 4
        self.hidden_dims = [32, 32]

        # self.memory = deque(maxlen=2000)

        # self.gamma = 0.85
        # self.epsilon = 1.0
        # self.epsilon_min = 0.01
        # self.epsilon_decay = 0.995
        # self.learning_rate = 0.005
        # self.tau = .125
        self.name = self.name()

        self.model = self.create_model()
        self.target_model = self.create_model()
        self.__build_train_fn()

    def create_model(self):
        model = Sequential()

        model.add(layers.Dense(self.input_dim, input_dim=self.input_dim))

        # for h_dim in self.hidden_dims:
        #     model.add(layers.Dense(h_dim))
        #     model.add(layers.Activation("relu"))

        model.add(layers.Dense(64))
        model.add(layers.Activation("relu"))

        model.add(layers.Dense(64))
        model.add(layers.Activation("tanh"))

        model.add(layers.Dropout(0.2))
        model.add(layers.GaussianNoise(0.1))

        model.add(layers.Dense(self.output_dim))
        model.add(layers.Activation("tanh"))

        model.summary()
        return model

    def __build_train_fn(self):
        """Create a train function
        It replaces `model.fit(X, y)` because we use the output of model and use it for training.
        For example, we need action placeholder
        called `action_one_hot` that stores, which action we took at state `s`.
        Hence, we can update the same action.
        This function will create
        `self.train_fn([state, action_one_hot, discount_reward])`
        which would train the model.
        """
        # action_prob_placeholder = self.model.output
        # action_onehot_placeholder = K.placeholder(shape=(None, self.output_dim),
        #                                           name="action_onehot")
        # discount_reward_placeholder = K.placeholder(shape=(None,),
        #                                             name="discount_reward")
        #
        # action_prob = K.sum(action_prob_placeholder * action_onehot_placeholder, axis=1)
        # log_action_prob = K.log(action_prob)
        #
        # loss = - log_action_prob * discount_reward_placeholder
        # loss = K.mean(loss)
        #
        # adam = optimizers.Adam(lr=0.001)
        #
        # updates = adam.get_updates(params=self.model.trainable_weights,
        #                            loss=loss)
        #
        # self.train_fn = K.function(inputs=[self.model.input,
        #                                    action_onehot_placeholder,
        #                                    discount_reward_placeholder],
        #                            outputs=[],
        #                            updates=updates)

        # action_prob_placeholder = self.model.output
        # action_onehot_placeholder = K.placeholder(shape=(None, self.output_dim),
        #                                           name="action_onehot")
        discount_reward_placeholder = K.placeholder(shape=(None,),
                                                    name="discount_reward")
        #
        # action_prob = K.sum(action_prob_placeholder * action_onehot_placeholder, axis=1)
        # log_action_prob = K.log(action_prob)

        loss = -  discount_reward_placeholder
        loss = K.mean(loss)

        adam = optimizers.Adam(lr=0.001)

        updates = adam.get_updates(params=self.model.trainable_weights,
                                   loss=loss)

        self.train_fn = K.function(inputs=[self.model.input,
                                           discount_reward_placeholder],
                                   outputs=[],
                                   updates=updates)

    def act(self, state):
        """Returns an action at given `state`
        Args:
            state (1-D or 2-D Array): It can be either 1-D array of shape (state_dimension, )
                or 2-D array shape of (n_samples, state_dimension)
        Returns:
            action: an integer action value ranging from 0 to (n_actions - 1)
        """
        shape = state.shape

        if len(shape) == 1:
            assert shape == (self.input_dim,), "{} != {}".format(shape, self.input_dim)
            state = np.expand_dims(state, axis=0)

        elif len(shape) == 2:
            assert shape[1] == self.input_dim, "{} != {}".format(shape, self.input_dim)

        else:
            raise TypeError("Wrong state shape is given: {}".format(state.shape))

        return np.squeeze(self.model.predict(state))

    def fit(self, S, A, R, new_S=None, done=None):
        """Train a network
        Args:
            S (2-D Array): `state` array of shape (n_samples, state_dimension)
            A (1-D Array): `action` array of shape (n_samples,)
                It's simply a list of int that stores which actions the agent chose
            R (1-D Array): `reward` array of shape (n_samples,)
                A reward is given after each action.
        """

        S = np.array([S])  # Embed S in array
        A = np.array([A])  # Embed A in array
        R = np.array([R])  # Embed R in array

        discount_reward = self.compute_discounted_R(R)

        print("Fitting with reward:", R)

        assert S.shape[1] == self.input_dim, "Input shape {} != expected input {}".format(S.shape[1], self.input_dim)
        assert A.shape[0] == S.shape[0], "Actions length {} != states length {}".format(A.shape[0], S.shape[0])
        assert A.shape[1] == self.output_dim, "Output shape {} != expected output shape{}".format(A.shape[1], self.output_dim)
        assert len(discount_reward.shape) == 1, "{} != 1".format(len(discount_reward.shape))

        # print("Calling train_fn with S: {}, action: {}, and discount_reward: {}".format(S, A, discount_reward))
        self.train_fn([S, A, discount_reward])

    def compute_discounted_R(self, R, discount_rate=.99):
        """Returns discounted rewards
        Args:
            R (1-D array): a list of `reward` at each time step
            discount_rate (float): Will discount the future value by this rate
        Returns:
            discounted_r (1-D array): same shape as input `R`
                but the values are discounted
        Examples:
            R = [1, 1, 1]
            compute_discounted_R(R, .99) # before normalization
            [1 + 0.99 + 0.99**2, 1 + 0.99, 1]
        """
        # discounted_r = np.zeros_like(R, dtype=np.float32)
        # running_add = 0
        # for t in reversed(range(len(R))):
        #     running_add = running_add * discount_rate + R[t]
        #     discounted_r[t] = running_add
        #
        # discounted_r -= discounted_r.mean() / discounted_r.std()
        #
        # return discounted_r

        return R  # No discount for now

    def save_model(self, fn):
        self.model.save(fn)

    @staticmethod
    def name():
        return 'Custom'
