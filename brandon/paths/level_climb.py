from sympy import symbols
import math


class LevelClimb:
    path = None
    name = None

    def __init__(self):
        self.path = self.path()
        self.name = self.name()

    @staticmethod
    def path():
        t = symbols('t')
        xoft = t
        yoft = 0.0000000001*t
        zoft = 1.0000000001*t + 2500
        soft = 52 + 0.00001*t
        return xoft, yoft, zoft, soft

    @staticmethod
    def name():
        return 'Level Climb'

