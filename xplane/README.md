# XPlaneLander

Using reinforcement landing to fly a plane in XPlane

[Documents and planning](https://drive.google.com/open?id=1pY9Zo-1Dq9uhCBTDZeSstygCDpLF6eBt)

[XPlane SDK References](http://www.xsquawkbox.net/xpsdk/docs/DataRefs.html)

## Creating an XPlane connection
```
import xpc
with xpc.XPlaneConnect() as client:
```

## Initializing the plane
Spawn the plane
```
#       Lat     Lon         Alt   Pitch Roll Yaw Gear
posi = [37.524, -122.06899, 2500, 0, 0, 0, 1]
client.sendPOSI(posi)
```

Set angle of attack, velocity, and orientation using the DATA command
```
data = [
    [18, 0, -998, 0, -998, -998, -998, -998, -998],
    [3, 130, 130, 130, 130, -998, -998, -998, -998],
    [16, 0, 0, 0, -998, -998, -998, -998, -998]
]
client.sendDATA(data)
```

Sends X-Plane data over the underlying UDP socket.
- Args:
  - data: An array of values representing data rows to be set. Each array in `data`
    - should have 9 elements, the first of which is a row number in the range (0-134), and the rest of which are the values to set for that data row.

## Controlling the Plane
These values control aileron, elevator, rudder and throttle positions
```
ctrl = [0.0, 0.0, 0.0, 0.8]
client.sendCTRL(ctrl)
```

## Controlling the Simulation
```
client.pauseSim(true) # Pause simulation
client.pauseSim(false) # Unpause simulation
```
- Simulation speed (`sim/time/sim_speed`) set int for multiplier

## Weather
- Rain percentage (`sim/weather/rain_percent`) [0.0-1.0]
- Wind Turbulence (`sim/weather/wind_turbulence_percent`) [0.0-1.0]
- Wind direction (`sim/weather/wind_altitude_msl_m[0]`) [direction in degrees from true north, clockwise]
- Wind altitude (`sim/weather/wind_altitude_msl_m[0]`) [center of wind layer in meters]
- Wind speed in knots (`sim/weather/wind_speed_kt[0]`)
- Wind shear in degrees (`sim/weather/shear_direction_degt[0]`)
- Thermals percent (`sim/weather/thermal_percent`)
- Thermal climb rate m/s (`sim/weather/thermal_rate_ms`)

## Input data
For this project, we have decided to give the AI all of the same values and data that an IFR pilot would have while flying in IMC conditions.

Flight characteristics:
- Attitude indicator 
    - Pitch (`the_vac_ind_deg`)
    - Roll 
    - Yaw 
- Altitude indicator (`sim/flightmodel/misc/h_ind`)
- Airspeed indicator (`sim/flightmodel/position/indicated_airspeed`)
- Roll indicator (`phi_vac_ind_deg`)
- Slip indicator (can't find it)
- Heading indicator (`psi_vac_ind_degm`)
- Glideslope indicator
    - Deflection from the aircraft to the glideslope (`sim/cockpit/radios/nav1_vdef_deg`)
    - Deflection from the aircraft to the course (localizer) (`sim/cockpit/radios/nav1_hdef_deg`)
    - OBS setting (`sim/cockpit/radios/nav1_obs_degt` or `nav1_obs_degm`)
    - Nav1 frequency tune (`sim/cockpit/radios/nav1_freq_hz`)
    - HSI selector int (`sim/cockpit/radios/HSI_selector`)


Other data:
- Engine Tachometer (in radians/sec for some reason) (`sim/flightmodel/engine/POINT_tacrad`)
- ATIS data
    - Wind direction
    - Wind velocity
    - Wind gusts
    - Density altitude
- Field elevation
- Downward G-forces (`sim/flightmodel/forces/g_nrml`)

## Approach assumptions
We've made some assumptions about the flight to simplify the AI:
- Fuel mix will always be full full-rich.
- Approaches will only be ILS on the front-course
- The AI will not have control of trim unless deemed necessary after a few attempts
- We'll use the auto-brake feature in XPlane
- We can modify simulation speed if necessary (`sim_speed`)

## Setting up the approach
We will need to manually configure a few things to set the airplane up for a specific approach
- Approach nav frequency (`sim/cockpit/radios/nav1_freq_hz`)
- Place the plane at the starting position
- Set HSI to nav1 (`sim/cockpit/switches/HSI_selector`)
- Turn on auto-brake (`auto_brake_settings`)
- Assign weather (`sim/weather/...`)

## Airport Info

RNAV Approach plate
![Approach plate](../approach.png)

- ICAO Identifier: KIND
- ILS Approach frequency: 111.75
- VOR (VHP) frequency: 116.30
- Field elevation: 747 feet