"""
This is the script that will organize the flight between each component
"""

from xplane import xpc, straight_and_level


def ex():
    with xpc.XPlaneConnect() as client:
        # Verify connection
        try:
            # If X-Plane does not respond to the request, a timeout error
            # will be raised.
            client.getDREF("sim/test/test_float")
        except:
            print("Error establishing connection to X-Plane.")
            print("Exiting...")
            return

        SAL = straight_and_level.StraightAndLevel(client)
        SAL.fly()


if __name__ == "__main__":
    ex()