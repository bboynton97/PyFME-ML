"""
Handles straight and level flight along a Victor airway
"""


class StraightAndLevel:
    def __init__(self, client):
        self.client = client

    def fly(self):
        print("\n----------\nBeginning straight and level flight\n----------")

        # Set starting position
        print("Setting position")
        #       Lat     Lon         Alt   Pitch Roll Yaw Gear
        posi = [39.799120, -86.367262, 762, 0, 0, 0, 1]  # Just south of the VOR
        self.client.sendPOSI(posi)

        # Reset rates from last flight
        self.client.sendDREF('sim/flightmodel/position//P', 0)  # Role rotation rate
        self.client.sendDREF('sim/flightmodel/position//Q', 0)  # Pitch rotation rate
        self.client.sendDREF('sim/flightmodel/position//R', 0)  # Yaw rotation rate

        # Reset velocities
        self.client.sendDREF("sim/flightmodel/position/local_vx", 50)
        self.client.sendDREF("sim/flightmodel/position/local_vy", 0)
        self.client.sendDREF("sim/flightmodel/position/local_vz", 0)

        # Reset forces from last flight
        # self.client.sendDREF('sim/operation/override/override_forces', 0)
        #
        # self.client.sendDREF('sim/flightmodel/forces/L_total', 0)  # Role
        # self.client.sendDREF('sim/flightmodel/forces/M_total', 0)  # Pitch
        # self.client.sendDREF('sim/flightmodel/forces/N_total', 0)  # Yaw

        # self.client.sendDREF('sim/flightmodel/forces/fside_total', 0)  # Role
        # self.client.sendDREF('sim/flightmodel/forces/fnrml_total', 0)  # Pitch
        # self.client.sendDREF('sim/flightmodel/forces/faxil_total', 0)  # Yaw
        # self.client.sendDREF('sim/flightmodel/forces/fnrml_aero', 10)  # lift

        # Set normal forces
        # self.client.sendDREF('sim/flightmodel/forces/fnrml_prop', 10000000)  # Yaw

        # Set controls to standard
        ctrl = [0.0, -1, 1.0, 0.8]  # aileron, elevator, rudder and throttle positions
        self.client.sendCTRL(ctrl)

        # Point south
        print("Setting orientation")

        # First int defines what is being modified
        data = [
            [18, 80, -998, 0, -998, -998, -998, -998, -998],
            [3, 130, 130, 130, 130, -998, -998, -998, -998],
            [16, 0, 0, 0, -998, -998, -998, -998, -998]
        ]
        self.client.sendDATA(data)

        self.set_radios()

        return

    def set_radios(self):
        # Setting radios to VOR
        self.client.sendDREF('sim/cockpit/radios/nav1_freq_hz', 11630)

        # Set glidescope to NAV
        self.client.sendDREF('sim/cockpit/switches/HSI_selector', 0)

        # Set OBS "From 180"
        self.client.sendDREF('sim/cockpit/radios/nav1_obs_degt', 355)  # for some reason 355 is north

        print("Set radios")

