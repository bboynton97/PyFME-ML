from xplane import xpc


def ex():
    print("X-Plane Connect example script")
    print("Setting up simulation")
    with xpc.XPlaneConnect() as client:
        # Verify connection
        try:
            # If X-Plane does not respond to the request, a timeout error
            # will be raised.
            client.getDREF("sim/test/test_float")
        except:
            print("Error establishing connection to X-Plane.")
            print("Exiting...")
            return

        # (get) Latitude, Longitude, Altitude
        lat_dref = "sim/flightmodel/position/latitude"
        lon_dref = "sim/flightmodel/position/longitude"
        alt_dref = "sim/flightmodel/position/elevation"

        # (get/set) Position DREFs
        x_dref = "sim/flightmodel/position/local_x"
        y_dref = "sim/flightmodel/position/local_y"
        z_dref = "sim/flightmodel/position/local_z"

        # (get/set) Velocity DREFs
        vx_dref = "sim/flightmodel/position/local_vx"
        vy_dref = "sim/flightmodel/position/local_vy"
        vz_dref = "sim/flightmodel/position/local_vz"

        # (get/set) Acceleration DREFs
        ax_dref = "sim/flightmodel/position/local_ax"
        ay_dref = "sim/flightmodel/position/local_ay"
        az_dref = "sim/flightmodel/position/local_az"

        # (get/set) Pitch, Roll, Heading
        theta_dref = "sim/flightmodel/position/theta"
        phi_dref = "sim/flightmodel/position/phi"
        psi_dref = "sim/flightmodel/position/psi"

        # (get/set) Roll, Pitch, Yaw Rotation Rates
        P_dref = "sim/flightmodel/position/P" # Roll
        Q_dref = "sim/flightmodel/position/Q" # Pitch
        R_dref = "sim/flightmodel/position/Q" # Yaw

        # Store initial values
        init_lat = client.getDREF(lat_dref)
        init_lon = client.getDREF(lon_dref)
        init_alt = client.getDREF(alt_dref)
        init_x = client.getDREF(x_dref)
        init_y = client.getDREF(y_dref)
        init_z = client.getDREF(z_dref)
        init_theta = client.getDREF(theta_dref)
        init_phi = client.getDREF(phi_dref)
        init_psi = client.getDREF(psi_dref)

        option = ""
        while option != "1":
            print("Menu:")
            print("   1) Exit")
            print("   2) Launch")
            print("   3) Rotate")
            print("   4) Spin")
            print("   5) POSI")
            print("   6) Start!")
            option = input()
            if (option == "2"):
                print("restarting")
                client.sendDREF(x_dref, init_x[0])
                client.sendDREF(y_dref, init_y[0] + 30)
                client.sendDREF(z_dref, init_z[0])
                client.sendDREF(vx_dref, 50)
                client.sendDREF(vy_dref, 10)
                client.sendDREF(vz_dref, 50)
            elif (option == "3"):
                client.sendDREF(y_dref, init_y[0] + 30)
                client.sendDREF(theta_dref, init_theta[0])
                client.sendDREF(phi_dref, init_phi[0])
                client.sendDREF(psi_dref, init_psi[0] + 30)
            elif (option == "4"):
                client.sendDREF(y_dref, init_y[0] + 30)
                client.sendDREF(R_dref, 90)
            elif (option == "5"):
                posi = [init_lat[0], init_lon[0], init_alt[0] + 1, 0, 0, 0, 1]
                client.sendPOSI(posi)
            elif (option == "6"):
                posi = [init_lat[0], init_lon[0], init_alt[0] + 30, 1, 0, 180, 1]
                client.sendPOSI(posi)
                client.sendDREF(vz_dref, 100)

        print("End of Python client example")


if __name__ == "__main__":
    ex()
