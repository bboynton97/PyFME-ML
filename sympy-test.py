import numpy as np
import math
from math import atan
from sympy import sqrt, lambdify, symbols, solve, sin, cos
from sympy.solvers.solveset import linsolve
from sympy.plotting import plot
from sympy.plotting import plot3d_parametric_line

show_graphs = False

x, y, z, t = symbols('x y z t')

# NOTE: sympy freaks out if one of the parametric equations is a constant/number
#       If you need a constant C, do C + 0.000...0001*t to trick it into working

# Parametric equations
xt = t**2
yt = t
zt = 1.1*t

xt_prime = xt.diff(t)
yt_prime = yt.diff(t)
zt_prime = zt.diff(t)

xt_prime_prime = xt_prime.diff(t)
yt_prime_prime = yt_prime.diff(t)
zt_prime_prime = zt_prime.diff(t)

print("x(t): ", xt, "\n")
print("x'(t) = ", xt_prime, "\n")
print("x''(t) = ", xt_prime_prime, "\n")

lxt = lambdify(t, xt)
lyt = lambdify(t, yt)
lzt = lambdify(t, zt)

lxt_prime = lambdify(t, xt_prime)
lyt_prime = lambdify(t, yt_prime)
lzt_prime = lambdify(t, zt_prime)

lxt_prime_prime = lambdify(t, xt_prime_prime)
lyt_prime_prime = lambdify(t, yt_prime_prime)
lzt_prime_prime = lambdify(t, zt_prime_prime)

plot3d_parametric_line((xt, yt, zt, (t, 0, 8)), show=show_graphs)

# Distance equation
distance = sqrt((xt - x)**2 + (yt - y)**2 + (zt - z)**2)
distance_prime = distance.diff(t)
distance_prime_prime = distance_prime.diff(t)

print("d(x, y, z, t): ", distance, "\n")
print("d'(x, y, z, t) = ", distance_prime, "\n")
print("d''(x, y, z, t) = ", distance_prime_prime, "\n")

d = lambdify((x, y, z, t), distance)
dp = lambdify((x, y, z, t), distance_prime)
dpp = lambdify((x, y, z, t), distance_prime_prime)

# Example graph
distance_ex = sqrt((xt - 1)**2 + (yt - 1)**2 + (zt -2)**2)
distance_prime_ex = distance_ex.diff(t)
distance_prime_prime_ex = distance_prime_ex.diff(t)
dmin, dmax = -8, 8
plot(
    (distance_ex, (t, dmin, dmax)),
    (distance_prime_ex, (t, dmin, dmax)),
    (distance_prime_prime_ex, (t, dmin, dmax)),
    show=show_graphs
)

# Finding minimum distance
extreme_values = solve(distance_prime, t)
extreme_value_equation = extreme_values[0] # TODO: needs to be more robust
print("Extreme value equation: ", extreme_value_equation)

ev = lambdify((x, y, z), extreme_value_equation)
evt = ev(1, 1, 2)
if dpp(1, 1, 2, evt) > 0:
    print(evt, "is a minimum")
elif dpp(1, 1, 2, evt) < 0:
    print(evt, "is a maximum")
else:
    print(evt, "is a saddle point?")

# NOTE: if there is a continuous range of solutions, sympy returns an empty set
solution_range = solve(sqrt(sin(t)**2 + cos(t)**2) - 1, t)
print(solution_range)

# Vectors at extreme value t (evt)
ext, eyt, ezt = lxt(evt), lyt(evt), lzt(evt)
extp, eytp, eztp = lxt_prime(evt), lyt_prime(evt), lzt_prime(evt)
extpp, eytpp, eztpp = lxt_prime_prime(evt), lyt_prime_prime(evt), lzt_prime_prime(evt)
print("\n\n (", ext, ",", eyt, ",", ezt, ")")
print("(", extp, ",", eytp, ",", eztp, ")")
print("(", extpp, ",", eytpp, ",", eztpp, ")")

# Path angles TODO: handle divide by 0
ppsi = atan(eytp/extp)
ph = sqrt(extp**2 + eytp**2)
ptheta = atan(eztp/ph)

# Aircraft angles + speed TODO: handle divide by 0
avx, avy, avz = 4.2, 0.8, 0.9
aspeed = sqrt(avx**2 + avy**2 + avz**2)
apsi = atan(avy/avx)
ah = sqrt(avx**2 + avy**2)
atheta = atan(avz/ah)

print("\nPath angle (theta, psi): (", ptheta, ",", ppsi, ")")
print("Aircraft angle (theta, psi): (", atheta, ",", apsi, ")")

# Displacement
slope = eytp / extp
line = slope * (x - ext) - y + eyt
linep = -1/slope * (x - 1) - y + 1
intersection, = linsolve([line, linep], (x, y))
print("Intersection: ", intersection)

pqd = sqrt((ext - intersection[0])**2 + (eyt - intersection[1])**2)

pointr = (1, 1)
qrd = sqrt((1 - intersection[0])**2 + (1 - intersection[1])**2)

deltz = abs(2 - ezt)

delta_x = sqrt(pqd**2 + qrd**2) # TODO: account for direction (+/-)
delta_y = sqrt(pqd**2 + deltz**2) # TODO: account for direction (+/-)
print("delta_x:", delta_x, " delta_y:", delta_y)
