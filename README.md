# PyFML - Machine Learning

The goal of this repo is to create a machine learning model that can successfully fly a plane on a given path.
  - Straight-and-level
  - Along a turn
  - In an approach
  - To a touchdown on a runway 

### Dependencies
  - Our [OpenAI Gym](https://gitlab.com/zbalda/gym-pyfme/-/tree/master/)
  - The physics engine [PyFME](https://github.com/AeroPython/PyFME)

## Setup

First, install the OpenAI Gym. This will be the "game" that our AI trains on.

Second, install PyFME: `pip install git+https://github.com/AeroPython/PyFME.git`

## Repo Structure

The two folders, `brandon` and `zachary` represent the work done by each respectively. Both attempt to build machine learning models that successfully perform the maneuvers outlined in the gym.

Shared resources will be at the root directory.

## Airport Info

RNAV Approach plate  
![Approach plate](approach.png)

- Field elevation: 747 feet
- Following coordinates
    - KNIGT (39.67degN/-86.20degW)
    - JAANE (39.60degN/-86.29degW)
    - KYYLL (39.59degN/-86.34degW)
    - HUUPS (39.65degN/-86.36degW)
    - Threshold (39.70degN/-86.30degW)