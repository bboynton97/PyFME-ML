import gym
import gym_pyfme

import math
import time
from sympy import symbols
import matplotlib.pyplot as plt
from pynput import keyboard
from pynput.keyboard import Key, Listener

render = False
print("\nKEYBOARD CONTROLS\n")
print("   w -> elevator forward")
print("   a -> aileron counterclockwise")
print("   s -> elevator back")
print("   d -> aileron clockwise\n")
print("   i -> throttle up")
print("   j -> rudder left")
print("   k -> throttle down")
print("   l -> rudder right\n")
print("   g -> end the simulation early\n")
input = input("Press Enter to Start (type 'r' to start with rendering): ")
if input == "r": render = True

key_values = ["'w'", "'a'", "'s'", "'d'", "'i'", "'j'", "'k'", "'l'"]
key_states = [False, False, False, False, False, False, False, False]

quit_value = ["'g'"]
quit_state = [False]

def on_press(key):
    key_str = '{0}'.format(key)
    if key_str in quit_value:
        index = quit_value.index(key_str)
        quit_state[index] = True
    if key_str in key_values:
        index = key_values.index(key_str)
        key_states[index] = True

def on_release(key):
    key_str = '{0}'.format(key)
    if key_str in key_values:
        index = key_values.index(key_str)
        key_states[index] = False

# Collect events until released
listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()

def get_control_adjustments():
    elevator_adjustment = 0
    aileron_adjustment = 0
    rudder_adjustment = 0
    throttle_adjustment = 0

    if key_states[0]:  # w
        elevator_adjustment += 1
    elif key_states[2]:  # s
        elevator_adjustment -= 1
    elif key_states[1]:  # a
        aileron_adjustment -= 1
    elif key_states[3]:  # d
        aileron_adjustment += 1
    elif key_states[5]:  # j
        rudder_adjustment -= 1
    elif key_states[7]:  # l
        rudder_adjustment += 1
    elif key_states[4]:  # i
        throttle_adjustment += 1
    elif key_states[6]:  # k
        throttle_adjustment -= 10

    return [elevator_adjustment, aileron_adjustment, rudder_adjustment, throttle_adjustment]


env = gym.make('PyFME-v0')

# Parametric equations for path
t = symbols('t')
env.create_path(
    t,
    0.0000000001*t,
    0.0000000001*t + 2500,
    52 + 0.00001*t
)

init_state = env.reset()
for i in range(1, 200):
    observation, reward, done, info = env.step(get_control_adjustments())
    env.print_state()
    if render: env.render()
    if done: break
    if quit_state[0]: break
results = env.get_results()
if render: env.export_render()
env.close()

kwargs = {'marker': '.',
          'subplots': True,
          'sharex': True,
          'figsize': (12, 20)}
results.plot(
    y=['elevator', 'aileron', 'rudder', 'thrust', 'x_earth', 'y_earth', 'height', 'theta', 'phi', 'psi', 'v_down',
       'v_east', 'v_north', 'u', 'v', 'w', 'TAS'], **kwargs)
plt.show()
